#include <iostream>
#include "palisade.h"


int main(){
    lbcrypto::CryptoContext<lbcrypto::DCRTPoly> cc = 
        lbcrypto::CryptoContextFactory<lbcrypto::DCRTPoly>::genCryptoContextBGVrnsWithParamsGen
           (8192, 2, 2, 0, OPTIMIZED, 1, 1, lbcrypto::GHS);

    cc->Enable(ENCRYPTION);
    cc->Enable(SHE);
    cc->Enable(LEVELEDSHE);

    
    lbcrypto::LPKeyPair<lbcrypto::DCRTPoly> keyPair = cc->KeyGen();
    cc->EvalMultKeyGen(keyPair.secretKey);

    std::vector<int32_t> indexList(1);
    for  (usint i=0; i<1; i++){
        indexList[i] = 1;
    }

    cc->EvalAtIndexKeyGen(keyPair.secretKey, indexList);

    std::vector<int64_t> vectorOfInts1 = {1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1};  
    std::vector<int64_t> vectorOfInts2 = {1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 1};  

    auto plaintext1 = cc->MakeCoefPackedPlaintext(vectorOfInts1);
    auto plaintext2 = cc->MakeCoefPackedPlaintext(vectorOfInts2);

    auto ciphertext1 = cc->Encrypt(keyPair.publicKey, plaintext1);
    auto ciphertext2 = cc->Encrypt(keyPair.publicKey, plaintext2);

    auto ciphertextMul = cc->EvalMult(ciphertext1, ciphertext2);

    lbcrypto::Plaintext plaintextres;
    cc->Decrypt(keyPair.secretKey, ciphertextMul, &plaintextres);
    plaintextres->SetLength(vectorOfInts1.size());
    std::cout<<"plaintextMul: "<<plaintextres<<std::endl;

    
    int ct = 0 ;
    while (ct<10) {
        auto ciphertext3 = cc->EvalAtIndex(ciphertextMul, 1);
        lbcrypto::Plaintext plaintext3;
        cc->Decrypt(keyPair.secretKey, ciphertext3, &plaintext3);
        plaintext3->SetLength(vectorOfInts1.size());
        std::cout<<"Plaintext3: "<<plaintext3<<std::endl;
        ct++;
    }

    return 0;
}